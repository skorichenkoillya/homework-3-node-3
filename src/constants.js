const FIRST_USER_SYMBOL = "x";
const SECOND_USER_SYMBOL = "o";

module.exports = {
    FIRST_USER_SYMBOL,
    SECOND_USER_SYMBOL
}