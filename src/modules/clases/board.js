const {Cell} = require('./cell');
const {transpose3d} = require('../helpers/transpose3d')
const {checkRowHasSameValues} = require('../helpers/checkRowHasSameValues')

class Board {


    constructor() {
        const grid = [[], [], []];
        this.grid = grid.reduce((accumulator) => {
            const row = [];
            for (let i = 0; i<3; i++) {
                row.push((new Cell()))
            }
            return [...accumulator, row]
        }, [])
    }

    output() {
        process.stdout.write(`___________\n`)
        this.grid.map(row => {
            process.stdout.write(`|`)
            row.map(cell => {
                process.stdout.write(`[${cell.value}]`)
            })
            process.stdout.write(`|\n`)
        })
        process.stdout.write(`-----------\n`)
    }

    setCellValue(x, y, value) {
        const cell = this.grid[x][y];
        cell.value = value;
        return cell.collapse;
    }

    checkVertical() {
        return checkRowHasSameValues(this.grid)
    }

    checkHorizontal() {
        return checkRowHasSameValues(transpose3d(this.grid))
    }

    checkDiagonal() {
        const checkLeftToRight = this.checkDiagonalLeftToRight()
        const checkRightToLeft = this.checkDiagonalRightToLeft()
        return checkLeftToRight || checkRightToLeft;
    }

    checkDiagonalLeftToRight() {
        const firstCell = this.grid[0][0];
        let value = firstCell.value;
        for (let i=0; i < this.grid.length; i++){
            if (this.grid[i][i].value !== value){
                return  null;
            }
        }
        return firstCell;
    }

    checkDiagonalRightToLeft() {
        const firstCell = this.grid[0][2];
        let value = firstCell.value;
        for (let i=0; i < this.grid.length; i++){
            if (this.grid[this.grid.length-1 - i][i].value !== value){
                return  null;
            }
        }
        return firstCell;
    }

}

module.exports = {
    Board
}