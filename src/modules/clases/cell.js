class Cell {
    constructor() {
        this._value = ' '
        this.empty = true
        this.collapse = false
    }

    get value() {
        return this._value;
    }

    set value(symbol) {
        if (this.empty) {
            this.empty = false;
            this._value = symbol;
        } else {
            this.collapse = true
        }
    }
}

module.exports = {
    Cell
}