const readline = require("readline");
const {Board} = require('./board');
const {Player} = require('./player');
const {FIRST_USER_SYMBOL, SECOND_USER_SYMBOL} = require('../../constants');

const rl = readline.createInterface({
    output: process.stdout,
    input: process.stdin
})

class Game {
    constructor() {
        this.board = new Board();
        this.firstPlayer = new Player(FIRST_USER_SYMBOL);
        this.secondPlayer = new Player(SECOND_USER_SYMBOL);
        this.currentUserId = 1;
        this.currentPlayer = this.firstPlayer;
    }

    async play() {
        console.clear()
        let win = false;
        while (true){
            this.board.output();
            console.log(`User${this.currentUserId}: ${this.currentPlayer.symbol}`)
            try {
                const coordinate = await this.inputCoordinateAsync()
                let collapse = this.board.setCellValue(...coordinate, this.currentPlayer.symbol);
                win = this.board.checkVertical() || this.board.checkHorizontal() || this.board.checkDiagonal();
                if (win){
                    console.log(`User${this.currentUserId}(${this.currentPlayer.symbol}) WIN !!!`);

                    return rl.close();
                }
                if (!collapse){
                    this.togglePlayers();
                } else {
                    console.log('Cell is not empty')
                }
            } catch (e) {
                console.log(e.message)
                await new Promise(resolve => setTimeout(resolve, 1500));
            }
            console.clear()
        }
    }

    togglePlayers(){
        if (this.currentUserId === 1) {
            this.currentUserId = 2;
            this.currentPlayer = this.secondPlayer;
        } else {
            this.currentUserId = 1;
            this.currentPlayer = this.firstPlayer;
        }
    }

    inputCoordinateAsync() {
        return new Promise((resolve, reject) => {
            rl.question('x,y (0-2): ', (data) => {
                const coordinate = data.trim().split(',');
                this.validate(data, reject);
                resolve(coordinate);
            });
        })
    }

    validate(data, reject) {
        if (!data.match(/\d,\d/)){
            return reject(new Error('Invalid format, expected *,*'));
        }
        const coordinate = data.trim().split(',');
        if (coordinate.length !== 2){
            return reject(new Error('Coordinates is required'));
        }

        if (parseInt(coordinate[0]) >= this.board.grid.length || parseInt(coordinate[0]) < 0 ){
            return reject(new Error('Coordinates "x" is out of range'));
        }

        if (parseInt(coordinate[1]) >= this.board.grid.length || parseInt(coordinate[1]) < 0 ){
            return reject(new Error('Coordinates "y" is out of range'));
        }

        coordinate.map(value => {
            if (!value.match(/^\d$/)){
                return reject(new Error('Coordinate must be number'));
            }
        })
    }

}

module.exports = {
    Game
}