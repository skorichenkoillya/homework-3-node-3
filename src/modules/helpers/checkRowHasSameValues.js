function checkRowHasSameValues(grid) {
    let cell = null;
    grid.some((row, roIndex) => {
        const cellWithSameValues = row.reduce((accumulator, cell, cellIndex) => {
            if (cell.value !== ' ' && cell.value === accumulator.value  ) {
                return {
                    value: cell.value,
                    index: cellIndex
                }
            }
            return {};
        }, {
            value: row[0].value,
            index: 0
        });
        if (cellWithSameValues.value) {
            return cell = grid[roIndex][cellWithSameValues.index]
        }
    })
    return cell;
}

module.exports = {
    checkRowHasSameValues
}